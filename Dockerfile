FROM golang:1.22-alpine

WORKDIR /usr/src/app

ARG bin_path="first_api"

# pre-copy/cache go.mod for pre-downloading dependencies and only redownloading them in subsequent builds if they change
COPY go.mod go.sum ./
RUN go mod download && go mod verify

COPY . .
RUN go build -v -o /usr/local/bin/${bin_path} ./cmd/${bin_path}

WORKDIR /usr/local/bin

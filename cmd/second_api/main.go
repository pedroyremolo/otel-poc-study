package main

import (
	"os"
	"time"

	"github.com/gorilla/mux"
	"github.com/rs/zerolog"
	"go.opentelemetry.io/contrib/instrumentation/github.com/gorilla/mux/otelmux"

	"gitlab.com/pedroyremolo/telemetry-study/pkg"
)

const serviceName = "secondAPI"

func main() {
	logger := zerolog.New(os.Stdout)
	logger.Info().Msg("initializing second api")
	shutdown, err := pkg.InitProvider(logger, serviceName)
	if err != nil {
		logger.Panic().Err(err).Msg("unable to initialize provider")
	}
	defer shutdown()

	api := pkg.NewAPI(logger)

	router := Router(api)

	pkg.ListenAndServe(":3001", logger, router, 5*time.Second, 5*time.Second)
}

func Router(api pkg.API) *mux.Router {
	r := mux.NewRouter()
	otelMid := otelmux.Middleware(serviceName)
	r.Use(otelMid)

	r.HandleFunc("/regular-with-error", api.RegularWithError)
	r.HandleFunc("/not-distributed", api.DistributedReceiverHandler)
	r.HandleFunc("/distributed", api.DistributedReceiverHandler)

	return r
}

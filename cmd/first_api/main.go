package main

import (
	"os"
	"time"

	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/rs/zerolog"
	"go.opentelemetry.io/contrib/instrumentation/github.com/gorilla/mux/otelmux"

	"gitlab.com/pedroyremolo/telemetry-study/pkg"
)

const serviceName = "firstAPI"

func main() {
	logger := zerolog.New(os.Stdout)
	logger.Info().Msg("initializing first api")
	shutdown, err := pkg.InitProvider(logger, serviceName)
	if err != nil {
		logger.Panic().Err(err).Msg("failed to initialize provider")
	}
	defer shutdown()

	api := pkg.NewAPI(logger)

	router := Router(api)

	pkg.ListenAndServe(":3000", logger, router, 60*time.Second, 60*time.Second)
}

func Router(api pkg.API) *mux.Router {
	r := mux.NewRouter()
	r.Handle("/metrics", promhttp.Handler())

	otelMid := otelmux.Middleware(serviceName)
	appRoute := r.PathPrefix("/api").Subrouter()
	appRoute.Use(otelMid)

	appRoute.HandleFunc("/regular", api.RegularHandler)
	appRoute.HandleFunc("/regular-span", api.RegularWithSpan)
	appRoute.HandleFunc("/distributed", api.DistributedSenderHandler)
	return r
}

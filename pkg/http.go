package pkg

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"net/http/httputil"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/rs/zerolog"
	"go.opentelemetry.io/contrib/instrumentation/net/http/otelhttp"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/codes"
	"go.opentelemetry.io/otel/trace"
)

func ListenAndServe(endpoint string, log zerolog.Logger, handler http.Handler, writeTimeout time.Duration, readTimeout time.Duration) {
	srv := &http.Server{
		Handler:      handler,
		Addr:         endpoint,
		WriteTimeout: writeTimeout,
		ReadTimeout:  readTimeout,
	}

	c := make(chan os.Signal, 1)
	idleConnections := make(chan struct{})
	signal.Notify(c, os.Interrupt, syscall.SIGINT)
	go func() {
		<-c
		// create context with timeout
		ctx, cancel := context.WithTimeout(context.Background(), writeTimeout)
		defer cancel()

		// start http shutdown
		if err := srv.Shutdown(ctx); err != nil {
			log.Error().Msg("failed to shutdown server: " + err.Error())
		}

		close(idleConnections)
	}()

	log.Info().Msg("listening requests at " + endpoint)
	if err := srv.ListenAndServe(); err != nil && !errors.Is(err, http.ErrServerClosed) {
		log.Fatal().Msg("failed to listen and serve: " + err.Error())
	}

	log.Info().Msg("Waiting idle connections...")
	<-idleConnections
	log.Info().Msg("Bye bye")
}

type API struct {
	Tracer trace.Tracer
	Log    zerolog.Logger

	distributedBaseURL string
}

func NewAPI(log zerolog.Logger) API {
	tracer := otel.Tracer("gitlab.com/pedroyremolo/telemetry-study/pkg")

	baseURL, ok := os.LookupEnv("DISTRIBUTED_API_BASE_URL")
	if !ok {
		log.Warn().Msg("missing DISTRIBUTED_API_BASE_URL environment variable")
		baseURL = "http://localhost:3001"
	}
	return API{Tracer: tracer, Log: log, distributedBaseURL: baseURL}
}

func (a API) DistributedSenderHandler(w http.ResponseWriter, r *http.Request) {
	resp, err := otelhttp.Get(r.Context(), fmt.Sprintf("%s/distributed", a.distributedBaseURL))
	if err != nil {
		_, _ = fmt.Fprintf(w, "failed to request second api")
		return
	}
	defer resp.Body.Close()
	_, _ = fmt.Fprintf(w, "distributed handler response first api")
}

func (a API) RegularHandler(w http.ResponseWriter, _ *http.Request) {
	_, _ = fmt.Fprintf(w, "regular handler response")
}

func (a API) RegularWithSpan(w http.ResponseWriter, r *http.Request) {
	_, span := a.Tracer.Start(r.Context(), "wait")

	time.Sleep(3 * time.Second)
	span.End()

	_, _ = fmt.Fprintf(w, "regular handler with span response")
}

type RegularWithErrorRequest struct {
	SleepingTime int `json:"sleeping_time_sec"`
}

type ErrorResponse struct {
	Error string `json:"err"`
}

const maxSleep = 5

func (a API) RegularWithError(w http.ResponseWriter, r *http.Request) {
	_, span := a.Tracer.Start(r.Context(), "body parsing")

	var request RegularWithErrorRequest
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		err = fmt.Errorf("failed to deserialize request body: %w", err)
		span.SetStatus(codes.Error, "regular with error failed")
		span.RecordError(err, trace.WithStackTrace(true))

		w.Header().Set("content-type", "application/json")
		w.WriteHeader(http.StatusBadRequest)
		_ = json.NewEncoder(w).Encode(ErrorResponse{Error: err.Error()})
		span.End()
		return
	}
	span.End()

	_, span2 := a.Tracer.Start(r.Context(), "sleeping time")
	if request.SleepingTime > maxSleep {
		err := fmt.Errorf("can't sleep more than %d", maxSleep)
		span.SetStatus(codes.Error, "regular with error failed")
		span.RecordError(err, trace.WithStackTrace(true))

		w.Header().Set("content-type", "application/json")
		w.WriteHeader(http.StatusBadRequest)
		_ = json.NewEncoder(w).Encode(ErrorResponse{Error: err.Error()})
		span2.End()
		return
	}

	time.Sleep(time.Duration(request.SleepingTime) * time.Second)
	span2.End()
}

func (a API) DistributedReceiverHandler(w http.ResponseWriter, r *http.Request) {
	reqBytes, _ := httputil.DumpRequest(r, false)
	a.Log.Info().Str("request", string(reqBytes)).Msg("received request")
	_, _ = fmt.Fprintf(w, "distributed handler response")
}
